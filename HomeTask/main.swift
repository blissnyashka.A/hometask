//
//  main.swift
//  HomeTask
//
//  Created by Anastasia Koldunova on 23.09.2022.
//

import Foundation



func getAvarage( _ array: [Int]) -> Double {
    let sumArray = array.reduce(0, +)
    return Double(sumArray)/Double(array.count)
}

func getGeometricalAverage( _ array: [Int]) -> Double {
    let multArray = array.reduce(1, *)
    return pow(Double(multArray), 1.0/Double(array.count))
}

func solveQuadraticEquation(a: Double, b: Double, c: Double) {
    let discriminant = b * b - (4 * a * c)
    let isHasSolution = discriminant >= 0
    let discrimimantSqrt = sqrt((discriminant))
    if isHasSolution {
        let x1 = (-b + discrimimantSqrt) / (2 * a)
        let x2 = (-b - discrimimantSqrt) / (2 * a)
        print("\(a)x^2+\(b)x+\(c) = 0 \nroot1=\(x1)\nroot2=\(x2)")
    } else {
        print("equation has no solution")
    }
}

let intArray = [2, 6, 4, 6, 8]


print(getAvarage(intArray))
print(getGeometricalAverage(intArray))
solveQuadraticEquation(a: 1, b: -6, c: -16)
